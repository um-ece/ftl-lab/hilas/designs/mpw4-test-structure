v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 340 290 340 323.75 { lab=GND}
N 340 260 393.75 260 { lab=SUB}
N -78.75 -1.25 -78.75 28.75 { lab=vpwr}
N 311.25 8.75 311.25 38.75 { lab=vpwr}
N 180 300 180 333.75 { lab=GND}
N 180 270 233.75 270 { lab=SUB}
N 30 300 30 333.75 { lab=GND}
N 30 270 83.75 270 { lab=SUB}
N -120 280 -120 313.75 { lab=GND}
N -120 250 -66.25 250 { lab=SUB}
N -160 250 -10 270 { lab=#net1}
N 140 270 300 260 { lab=#net2}
N 171.25 138.75 180 240 { lab=#net2}
N 30 240 51.25 138.75 { lab=#net1}
N 140 270 180 240 { lab=#net2}
N -10 270 30 240 { lab=#net1}
N 111.25 -51.25 111.25 -21.25 { lab=vpwr}
N 2.5 -75 111.25 -51.25 { lab=vpwr}
N 2.5 -75 171.25 108.75 { lab=vpwr}
N 2.5 -75 51.25 108.75 { lab=vpwr}
N 51.25 78.75 111.25 8.75 { lab=#net3}
N 111.25 8.75 171.25 78.75 { lab=#net3}
N -78.75 -1.25 2.5 -75 { lab=vpwr}
N 2.5 -75 311.25 8.75 { lab=vpwr}
N -120 220 -78.75 58.75 { lab=#net4}
N -118.75 28.75 -78.75 58.75 { lab=#net4}
N -78.75 58.75 271.25 38.75 { lab=#net4}
N 311.25 68.75 340 230 { lab=#net5}
N -305 265 -265 275 { lab=#net6}
N -305 160 -265 170 { lab=#net7}
N -265 110 71.25 -21.25 { lab=#net8}
N -265 215 71.25 -21.25 { lab=#net8}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/nfet_01v8.sym} 320 260 0 0 {name=M5
L=0.15
W=1.5
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {lab_pin.sym} 340 323.75 0 0 {name=l1 sig_type=std_logic lab=GND}
C {lab_pin.sym} 393.75 260 2 0 {name=l2 sig_type=std_logic lab=SUB}
C {lab_pin.sym} 2.5 -75 1 0 {name=l14 sig_type=std_logic lab=vpwr}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} -98.75 28.75 0 0 {name=M6
L=0.15
W=1.5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 91.25 -21.25 0 0 {name=M1
L=0.15
W=20
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 291.25 38.75 0 0 {name=M2
L=0.15
W=1.5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 31.25 108.75 0 0 {name=M3
L=0.15
W=20
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 151.25 108.75 0 0 {name=M4
L=0.15
W=20
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/nfet_01v8.sym} 160 270 0 0 {name=M7
L=0.15
W=1.5
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {lab_pin.sym} 180 333.75 0 0 {name=l3 sig_type=std_logic lab=GND}
C {lab_pin.sym} 233.75 270 2 0 {name=l4 sig_type=std_logic lab=SUB}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/nfet_01v8.sym} 10 270 0 0 {name=M8
L=0.15
W=1.5
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {lab_pin.sym} 30 333.75 0 0 {name=l5 sig_type=std_logic lab=GND}
C {lab_pin.sym} 83.75 270 2 0 {name=l6 sig_type=std_logic lab=SUB}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/nfet_01v8.sym} -140 250 0 0 {name=M9
L=0.15
W=1.5
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {lab_pin.sym} -120 313.75 0 0 {name=l7 sig_type=std_logic lab=GND}
C {lab_pin.sym} -66.25 250 2 0 {name=l8 sig_type=std_logic lab=SUB}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_var_hvt.sym} -265 140 0 0 {name=C1 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_var_hvt.sym} -265 245 0 0 {name=C2 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
