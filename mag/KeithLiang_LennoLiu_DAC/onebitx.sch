v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 350 170 350 203.75 { lab=GND}
N 350 140 403.75 140 { lab=SUB}
N 1.25 -41.25 2.5 -75 { lab=vpwr}
N 1.25 -41.25 1.25 -11.25 { lab=vpwr}
N 2.5 -75 131.25 -21.25 { lab=vpwr}
N -38.75 -11.25 91.25 8.75 { lab=#net1}
N 131.25 -21.25 131.25 8.75 { lab=vpwr}
N 2.5 -75 351.25 -41.25 { lab=vpwr}
N 351.25 -41.25 351.25 -11.25 { lab=vpwr}
N 310 140 311.25 -11.25 { lab=#net2}
N 131.25 38.75 211.25 108.75 { lab=#net3}
N 101.25 98.75 131.25 38.75 { lab=#net3}
N 2.5 -75 211.25 138.75 { lab=vpwr}
N 2.5 -75 101.25 128.75 { lab=vpwr}
N 61.25 128.75 310 140 { lab=#net2}
N 350 110 351.25 18.75 { lab=#net4}
N 171.25 138.75 350 110 { lab=#net4}
N -60 140 -38.75 -11.25 { lab=#net1}
N -100 190 -60 200 { lab=#net5}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/nfet_01v8.sym} 330 140 0 0 {name=M5
L=0.15
W=0.42
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {lab_pin.sym} 350 203.75 0 0 {name=l1 sig_type=std_logic lab=GND}
C {lab_pin.sym} 403.75 140 2 0 {name=l2 sig_type=std_logic lab=SUB}
C {lab_pin.sym} 2.5 -75 1 0 {name=l14 sig_type=std_logic lab=vpwr}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} -18.75 -11.25 0 0 {name=M6
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 111.25 8.75 0 0 {name=M1
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 331.25 -11.25 0 0 {name=M2
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 81.25 128.75 0 0 {name=M3
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 191.25 138.75 0 0 {name=M4
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_var_lvt.sym} -60 170 0 0 {name=C2 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
