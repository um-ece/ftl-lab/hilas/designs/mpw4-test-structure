v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 350 170 350 203.75 { lab=GND}
N 350 140 403.75 140 { lab=SUB}
N 1.25 -41.25 2.5 -75 { lab=vpwr}
N 1.25 -41.25 1.25 -11.25 { lab=vpwr}
N 2.5 -75 131.25 -21.25 { lab=vpwr}
N -38.75 -11.25 91.25 8.75 { lab=#net1}
N 131.25 -21.25 131.25 8.75 { lab=vpwr}
N 2.5 -75 351.25 -41.25 { lab=vpwr}
N 351.25 -41.25 351.25 -11.25 { lab=vpwr}
N 310 140 311.25 -11.25 { lab=#net2}
N 131.25 38.75 211.25 108.75 { lab=#net3}
N 101.25 98.75 131.25 38.75 { lab=#net3}
N 2.5 -75 211.25 138.75 { lab=vpwr}
N 2.5 -75 101.25 128.75 { lab=vpwr}
N 61.25 128.75 310 140 { lab=#net2}
N 350 110 351.25 18.75 { lab=#net4}
N 171.25 138.75 350 110 { lab=#net4}
N -60 140 -38.75 -11.25 { lab=#net1}
N -100 190 -60 200 { lab=#net5}
N 940 190 940 223.75 { lab=GND}
N 940 160 993.75 160 { lab=SUB}
N 591.25 -21.25 592.5 -55 { lab=vpwr}
N 591.25 -21.25 591.25 8.75 { lab=vpwr}
N 592.5 -55 721.25 -1.25 { lab=vpwr}
N 551.25 8.75 681.25 28.75 { lab=#net6}
N 721.25 -1.25 721.25 28.75 { lab=vpwr}
N 592.5 -55 941.25 -21.25 { lab=vpwr}
N 941.25 -21.25 941.25 8.75 { lab=vpwr}
N 900 160 901.25 8.75 { lab=#net7}
N 721.25 58.75 801.25 128.75 { lab=#net8}
N 691.25 118.75 721.25 58.75 { lab=#net8}
N 592.5 -55 801.25 158.75 { lab=vpwr}
N 592.5 -55 691.25 148.75 { lab=vpwr}
N 651.25 148.75 900 160 { lab=#net7}
N 940 130 941.25 38.75 { lab=#net9}
N 761.25 158.75 940 130 { lab=#net9}
N 530 160 551.25 8.75 { lab=#net6}
N 490 210 530 220 { lab=#net5}
N -200 120 -160 130 { lab=#net10}
N 450 100 490 110 { lab=#net11}
N 490 50 551.25 8.75 { lab=#net6}
N -160 70 -38.75 -11.25 { lab=#net1}
N 211.25 168.75 801.25 188.75 { lab=#net12}
N 101.25 158.75 691.25 178.75 { lab=#net13}
N -60 200 530 220 { lab=#net5}
N 1650 180 1650 213.75 { lab=GND}
N 1650 150 1703.75 150 { lab=SUB}
N 1301.25 -31.25 1302.5 -65 { lab=vpwr}
N 1301.25 -31.25 1301.25 -1.25 { lab=vpwr}
N 1302.5 -65 1431.25 -11.25 { lab=vpwr}
N 1261.25 -1.25 1391.25 18.75 { lab=#net14}
N 1431.25 -11.25 1431.25 18.75 { lab=vpwr}
N 1302.5 -65 1651.25 -31.25 { lab=vpwr}
N 1651.25 -31.25 1651.25 -1.25 { lab=vpwr}
N 1610 150 1611.25 -1.25 { lab=#net15}
N 1431.25 48.75 1511.25 118.75 { lab=#net16}
N 1401.25 108.75 1431.25 48.75 { lab=#net16}
N 1302.5 -65 1511.25 148.75 { lab=vpwr}
N 1302.5 -65 1401.25 138.75 { lab=vpwr}
N 1361.25 138.75 1610 150 { lab=#net15}
N 1650 120 1651.25 28.75 { lab=#net17}
N 1471.25 148.75 1650 120 { lab=#net17}
N 1240 150 1261.25 -1.25 { lab=#net14}
N 1200 200 1240 210 { lab=#net5}
N 2240 200 2240 233.75 { lab=GND}
N 2240 170 2293.75 170 { lab=SUB}
N 1891.25 -11.25 1892.5 -45 { lab=vpwr}
N 1891.25 -11.25 1891.25 18.75 { lab=vpwr}
N 1892.5 -45 2021.25 8.75 { lab=vpwr}
N 1851.25 18.75 1981.25 38.75 { lab=#net18}
N 2021.25 8.75 2021.25 38.75 { lab=vpwr}
N 1892.5 -45 2241.25 -11.25 { lab=vpwr}
N 2241.25 -11.25 2241.25 18.75 { lab=vpwr}
N 2200 170 2201.25 18.75 { lab=#net19}
N 2021.25 68.75 2101.25 138.75 { lab=#net20}
N 1991.25 128.75 2021.25 68.75 { lab=#net20}
N 1892.5 -45 2101.25 168.75 { lab=vpwr}
N 1892.5 -45 1991.25 158.75 { lab=vpwr}
N 1951.25 158.75 2200 170 { lab=#net19}
N 2240 140 2241.25 48.75 { lab=#net21}
N 2061.25 168.75 2240 140 { lab=#net21}
N 1830 170 1851.25 18.75 { lab=#net18}
N 1790 220 1830 230 { lab=#net5}
N 1100 130 1140 140 { lab=#net22}
N 1750 110 1790 120 { lab=#net23}
N 1790 60 1851.25 18.75 { lab=#net18}
N 1140 80 1261.25 -1.25 { lab=#net14}
N 1511.25 178.75 2101.25 198.75 { lab=#net12}
N 1401.25 168.75 1991.25 188.75 { lab=#net13}
N 1240 210 1830 230 { lab=#net5}
N 691.25 178.75 1401.25 168.75 { lab=#net13}
N 801.25 188.75 1511.25 178.75 { lab=#net12}
N 530 220 1240 210 { lab=#net5}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/nfet_01v8.sym} 330 140 0 0 {name=M5
L=0.15
W=0.42
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {lab_pin.sym} 350 203.75 0 0 {name=l1 sig_type=std_logic lab=GND}
C {lab_pin.sym} 403.75 140 2 0 {name=l2 sig_type=std_logic lab=SUB}
C {lab_pin.sym} 2.5 -75 1 0 {name=l14 sig_type=std_logic lab=vpwr}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} -18.75 -11.25 0 0 {name=M6
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 111.25 8.75 0 0 {name=M1
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 331.25 -11.25 0 0 {name=M2
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 81.25 128.75 0 0 {name=M3
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 191.25 138.75 0 0 {name=M4
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_var_lvt.sym} -60 170 0 0 {name=C2 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/nfet_01v8.sym} 920 160 0 0 {name=M7
L=0.15
W=0.42
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {lab_pin.sym} 940 223.75 0 0 {name=l3 sig_type=std_logic lab=GND}
C {lab_pin.sym} 993.75 160 2 0 {name=l4 sig_type=std_logic lab=SUB}
C {lab_pin.sym} 592.5 -55 1 0 {name=l5 sig_type=std_logic lab=vpwr}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 571.25 8.75 0 0 {name=M8
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 701.25 28.75 0 0 {name=M9
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 921.25 8.75 0 0 {name=M10
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 671.25 148.75 0 0 {name=M11
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 781.25 158.75 0 0 {name=M12
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_var_lvt.sym} 530 190 0 0 {name=C1 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_var_lvt.sym} -160 100 0 0 {name=C3 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_var_lvt.sym} 490 80 0 0 {name=C4 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/nfet_01v8.sym} 1630 150 0 0 {name=M13
L=0.15
W=0.42
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {lab_pin.sym} 1650 213.75 0 0 {name=l6 sig_type=std_logic lab=GND}
C {lab_pin.sym} 1703.75 150 2 0 {name=l7 sig_type=std_logic lab=SUB}
C {lab_pin.sym} 1302.5 -65 1 0 {name=l8 sig_type=std_logic lab=vpwr}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 1281.25 -1.25 0 0 {name=M14
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 1411.25 18.75 0 0 {name=M15
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 1631.25 -1.25 0 0 {name=M16
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 1381.25 138.75 0 0 {name=M17
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 1491.25 148.75 0 0 {name=M18
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_var_lvt.sym} 1240 180 0 0 {name=C5 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/nfet_01v8.sym} 2220 170 0 0 {name=M19
L=0.15
W=0.42
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {lab_pin.sym} 2240 233.75 0 0 {name=l9 sig_type=std_logic lab=GND}
C {lab_pin.sym} 2293.75 170 2 0 {name=l10 sig_type=std_logic lab=SUB}
C {lab_pin.sym} 1892.5 -45 1 0 {name=l11 sig_type=std_logic lab=vpwr}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 1871.25 18.75 0 0 {name=M20
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 2001.25 38.75 0 0 {name=M21
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 2221.25 18.75 0 0 {name=M22
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 1971.25 158.75 0 0 {name=M23
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 2081.25 168.75 0 0 {name=M24
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_var_lvt.sym} 1830 200 0 0 {name=C6 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_var_lvt.sym} 1140 110 0 0 {name=C7 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_var_lvt.sym} 1790 90 0 0 {name=C8 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
