v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 350 170 350 203.75 { lab=GND}
N 350 140 403.75 140 { lab=SUB}
N 1.25 -41.25 2.5 -75 { lab=vpwr}
N 1.25 -41.25 1.25 -11.25 { lab=vpwr}
N 2.5 -75 131.25 -21.25 { lab=vpwr}
N -38.75 -11.25 91.25 8.75 { lab=#net1}
N 131.25 -21.25 131.25 8.75 { lab=vpwr}
N 2.5 -75 351.25 -41.25 { lab=vpwr}
N 351.25 -41.25 351.25 -11.25 { lab=vpwr}
N 310 140 311.25 -11.25 { lab=#net2}
N 131.25 38.75 211.25 108.75 { lab=#net3}
N 101.25 98.75 131.25 38.75 { lab=#net3}
N 2.5 -75 211.25 138.75 { lab=vpwr}
N 2.5 -75 101.25 128.75 { lab=vpwr}
N 61.25 128.75 310 140 { lab=#net2}
N 350 110 351.25 18.75 { lab=#net4}
N 171.25 138.75 350 110 { lab=#net4}
N -60 140 -38.75 -11.25 { lab=#net1}
N -100 190 -60 200 { lab=#net5}
N 940 190 940 223.75 { lab=GND}
N 940 160 993.75 160 { lab=SUB}
N 591.25 -21.25 592.5 -55 { lab=vpwr}
N 591.25 -21.25 591.25 8.75 { lab=vpwr}
N 592.5 -55 721.25 -1.25 { lab=vpwr}
N 551.25 8.75 681.25 28.75 { lab=#net6}
N 721.25 -1.25 721.25 28.75 { lab=vpwr}
N 592.5 -55 941.25 -21.25 { lab=vpwr}
N 941.25 -21.25 941.25 8.75 { lab=vpwr}
N 900 160 901.25 8.75 { lab=#net7}
N 721.25 58.75 801.25 128.75 { lab=#net8}
N 691.25 118.75 721.25 58.75 { lab=#net8}
N 592.5 -55 801.25 158.75 { lab=vpwr}
N 592.5 -55 691.25 148.75 { lab=vpwr}
N 651.25 148.75 900 160 { lab=#net7}
N 940 130 941.25 38.75 { lab=#net9}
N 761.25 158.75 940 130 { lab=#net9}
N 530 160 551.25 8.75 { lab=#net6}
N 490 210 530 220 { lab=#net5}
N -200 120 -160 130 { lab=#net10}
N 450 100 490 110 { lab=#net11}
N 490 50 551.25 8.75 { lab=#net6}
N -160 70 -38.75 -11.25 { lab=#net1}
N 211.25 168.75 801.25 188.75 { lab=#net12}
N 101.25 158.75 691.25 178.75 { lab=#net13}
N -60 200 530 220 { lab=#net5}
N 1650 180 1650 213.75 { lab=GND}
N 1650 150 1703.75 150 { lab=SUB}
N 1301.25 -31.25 1302.5 -65 { lab=vpwr}
N 1301.25 -31.25 1301.25 -1.25 { lab=vpwr}
N 1302.5 -65 1431.25 -11.25 { lab=vpwr}
N 1261.25 -1.25 1391.25 18.75 { lab=#net14}
N 1431.25 -11.25 1431.25 18.75 { lab=vpwr}
N 1302.5 -65 1651.25 -31.25 { lab=vpwr}
N 1651.25 -31.25 1651.25 -1.25 { lab=vpwr}
N 1610 150 1611.25 -1.25 { lab=#net15}
N 1431.25 48.75 1511.25 118.75 { lab=#net16}
N 1401.25 108.75 1431.25 48.75 { lab=#net16}
N 1302.5 -65 1511.25 148.75 { lab=vpwr}
N 1302.5 -65 1401.25 138.75 { lab=vpwr}
N 1361.25 138.75 1610 150 { lab=#net15}
N 1650 120 1651.25 28.75 { lab=#net17}
N 1471.25 148.75 1650 120 { lab=#net17}
N 1240 150 1261.25 -1.25 { lab=#net14}
N 1200 200 1240 210 { lab=#net5}
N 2240 200 2240 233.75 { lab=GND}
N 2240 170 2293.75 170 { lab=SUB}
N 1891.25 -11.25 1892.5 -45 { lab=vpwr}
N 1891.25 -11.25 1891.25 18.75 { lab=vpwr}
N 1892.5 -45 2021.25 8.75 { lab=vpwr}
N 1851.25 18.75 1981.25 38.75 { lab=#net18}
N 2021.25 8.75 2021.25 38.75 { lab=vpwr}
N 1892.5 -45 2241.25 -11.25 { lab=vpwr}
N 2241.25 -11.25 2241.25 18.75 { lab=vpwr}
N 2200 170 2201.25 18.75 { lab=#net19}
N 2021.25 68.75 2101.25 138.75 { lab=#net20}
N 1991.25 128.75 2021.25 68.75 { lab=#net20}
N 1892.5 -45 2101.25 168.75 { lab=vpwr}
N 1892.5 -45 1991.25 158.75 { lab=vpwr}
N 1951.25 158.75 2200 170 { lab=#net19}
N 2240 140 2241.25 48.75 { lab=#net21}
N 2061.25 168.75 2240 140 { lab=#net21}
N 1830 170 1851.25 18.75 { lab=#net18}
N 1790 220 1830 230 { lab=#net5}
N 1100 130 1140 140 { lab=#net22}
N 1750 110 1790 120 { lab=#net23}
N 1790 60 1851.25 18.75 { lab=#net18}
N 1140 80 1261.25 -1.25 { lab=#net14}
N 1511.25 178.75 2101.25 198.75 { lab=#net12}
N 1401.25 168.75 1991.25 188.75 { lab=#net13}
N 1240 210 1830 230 { lab=#net5}
N 691.25 178.75 1401.25 168.75 { lab=#net13}
N 801.25 188.75 1511.25 178.75 { lab=#net12}
N 530 220 1240 210 { lab=#net5}
N 330 660 330 693.75 { lab=GND}
N 330 630 383.75 630 { lab=SUB}
N -18.75 448.75 -17.5 415 { lab=vpwr}
N -18.75 448.75 -18.75 478.75 { lab=vpwr}
N -17.5 415 111.25 468.75 { lab=vpwr}
N -58.75 478.75 71.25 498.75 { lab=#net24}
N 111.25 468.75 111.25 498.75 { lab=vpwr}
N -17.5 415 331.25 448.75 { lab=vpwr}
N 331.25 448.75 331.25 478.75 { lab=vpwr}
N 290 630 291.25 478.75 { lab=#net25}
N 111.25 528.75 191.25 598.75 { lab=#net26}
N 81.25 588.75 111.25 528.75 { lab=#net26}
N -17.5 415 191.25 628.75 { lab=vpwr}
N -17.5 415 81.25 618.75 { lab=vpwr}
N 41.25 618.75 290 630 { lab=#net25}
N 330 600 331.25 508.75 { lab=#net27}
N 151.25 628.75 330 600 { lab=#net27}
N -80 630 -58.75 478.75 { lab=#net24}
N -120 680 -80 690 { lab=#net5}
N 920 680 920 713.75 { lab=GND}
N 920 650 973.75 650 { lab=SUB}
N 571.25 468.75 572.5 435 { lab=vpwr}
N 571.25 468.75 571.25 498.75 { lab=vpwr}
N 572.5 435 701.25 488.75 { lab=vpwr}
N 531.25 498.75 661.25 518.75 { lab=#net28}
N 701.25 488.75 701.25 518.75 { lab=vpwr}
N 572.5 435 921.25 468.75 { lab=vpwr}
N 921.25 468.75 921.25 498.75 { lab=vpwr}
N 880 650 881.25 498.75 { lab=#net29}
N 701.25 548.75 781.25 618.75 { lab=#net30}
N 671.25 608.75 701.25 548.75 { lab=#net30}
N 572.5 435 781.25 648.75 { lab=vpwr}
N 572.5 435 671.25 638.75 { lab=vpwr}
N 631.25 638.75 880 650 { lab=#net29}
N 920 620 921.25 528.75 { lab=#net31}
N 741.25 648.75 920 620 { lab=#net31}
N 510 650 531.25 498.75 { lab=#net28}
N 470 700 510 710 { lab=#net5}
N -220 610 -180 620 { lab=#net32}
N 430 590 470 600 { lab=#net33}
N 470 540 531.25 498.75 { lab=#net28}
N -180 560 -58.75 478.75 { lab=#net24}
N 191.25 658.75 781.25 678.75 { lab=#net12}
N 81.25 648.75 671.25 668.75 { lab=#net13}
N -80 690 510 710 { lab=#net5}
N 1630 670 1630 703.75 { lab=GND}
N 1630 640 1683.75 640 { lab=SUB}
N 1281.25 458.75 1282.5 425 { lab=vpwr}
N 1281.25 458.75 1281.25 488.75 { lab=vpwr}
N 1282.5 425 1411.25 478.75 { lab=vpwr}
N 1241.25 488.75 1371.25 508.75 { lab=#net34}
N 1411.25 478.75 1411.25 508.75 { lab=vpwr}
N 1282.5 425 1631.25 458.75 { lab=vpwr}
N 1631.25 458.75 1631.25 488.75 { lab=vpwr}
N 1590 640 1591.25 488.75 { lab=#net35}
N 1411.25 538.75 1491.25 608.75 { lab=#net36}
N 1381.25 598.75 1411.25 538.75 { lab=#net36}
N 1282.5 425 1491.25 638.75 { lab=vpwr}
N 1282.5 425 1381.25 628.75 { lab=vpwr}
N 1341.25 628.75 1590 640 { lab=#net35}
N 1630 610 1631.25 518.75 { lab=#net37}
N 1451.25 638.75 1630 610 { lab=#net37}
N 1220 640 1241.25 488.75 { lab=#net34}
N 1180 690 1220 700 { lab=#net5}
N 2220 690 2220 723.75 { lab=GND}
N 2220 660 2273.75 660 { lab=SUB}
N 1871.25 478.75 1872.5 445 { lab=vpwr}
N 1871.25 478.75 1871.25 508.75 { lab=vpwr}
N 1872.5 445 2001.25 498.75 { lab=vpwr}
N 1831.25 508.75 1961.25 528.75 { lab=#net38}
N 2001.25 498.75 2001.25 528.75 { lab=vpwr}
N 1872.5 445 2221.25 478.75 { lab=vpwr}
N 2221.25 478.75 2221.25 508.75 { lab=vpwr}
N 2180 660 2181.25 508.75 { lab=#net39}
N 2001.25 558.75 2081.25 628.75 { lab=#net40}
N 1971.25 618.75 2001.25 558.75 { lab=#net40}
N 1872.5 445 2081.25 658.75 { lab=vpwr}
N 1872.5 445 1971.25 648.75 { lab=vpwr}
N 1931.25 648.75 2180 660 { lab=#net39}
N 2220 630 2221.25 538.75 { lab=#net41}
N 2041.25 658.75 2220 630 { lab=#net41}
N 1810 660 1831.25 508.75 { lab=#net38}
N 1770 710 1810 720 { lab=#net5}
N 1080 620 1120 630 { lab=#net42}
N 1730 600 1770 610 { lab=#net43}
N 1770 550 1831.25 508.75 { lab=#net38}
N 1120 570 1241.25 488.75 { lab=#net34}
N 1491.25 668.75 2081.25 688.75 { lab=#net12}
N 1381.25 658.75 1971.25 678.75 { lab=#net13}
N 1220 700 1810 720 { lab=#net5}
N 671.25 668.75 1381.25 658.75 { lab=#net13}
N 781.25 678.75 1491.25 668.75 { lab=#net12}
N 510 710 1220 700 { lab=#net5}
N 310 1150 310 1183.75 { lab=GND}
N 310 1120 363.75 1120 { lab=SUB}
N -38.75 938.75 -37.5 905 { lab=vpwr}
N -38.75 938.75 -38.75 968.75 { lab=vpwr}
N -37.5 905 91.25 958.75 { lab=vpwr}
N -78.75 968.75 51.25 988.75 { lab=#net44}
N 91.25 958.75 91.25 988.75 { lab=vpwr}
N -37.5 905 311.25 938.75 { lab=vpwr}
N 311.25 938.75 311.25 968.75 { lab=vpwr}
N 270 1120 271.25 968.75 { lab=#net45}
N 91.25 1018.75 171.25 1088.75 { lab=#net46}
N 61.25 1078.75 91.25 1018.75 { lab=#net46}
N -37.5 905 171.25 1118.75 { lab=vpwr}
N -37.5 905 61.25 1108.75 { lab=vpwr}
N 21.25 1108.75 270 1120 { lab=#net45}
N 310 1090 311.25 998.75 { lab=#net47}
N 131.25 1118.75 310 1090 { lab=#net47}
N -100 1120 -78.75 968.75 { lab=#net44}
N -140 1170 -100 1180 { lab=#net5}
N 900 1170 900 1203.75 { lab=GND}
N 900 1140 953.75 1140 { lab=SUB}
N 551.25 958.75 552.5 925 { lab=vpwr}
N 551.25 958.75 551.25 988.75 { lab=vpwr}
N 552.5 925 681.25 978.75 { lab=vpwr}
N 511.25 988.75 641.25 1008.75 { lab=#net48}
N 681.25 978.75 681.25 1008.75 { lab=vpwr}
N 552.5 925 901.25 958.75 { lab=vpwr}
N 901.25 958.75 901.25 988.75 { lab=vpwr}
N 860 1140 861.25 988.75 { lab=#net49}
N 681.25 1038.75 761.25 1108.75 { lab=#net50}
N 651.25 1098.75 681.25 1038.75 { lab=#net50}
N 552.5 925 761.25 1138.75 { lab=vpwr}
N 552.5 925 651.25 1128.75 { lab=vpwr}
N 611.25 1128.75 860 1140 { lab=#net49}
N 900 1110 901.25 1018.75 { lab=#net51}
N 721.25 1138.75 900 1110 { lab=#net51}
N 490 1140 511.25 988.75 { lab=#net48}
N 450 1190 490 1200 { lab=#net5}
N -240 1100 -200 1110 { lab=#net52}
N 410 1080 450 1090 { lab=#net53}
N 450 1030 511.25 988.75 { lab=#net48}
N -200 1050 -78.75 968.75 { lab=#net44}
N 171.25 1148.75 761.25 1168.75 { lab=#net12}
N 61.25 1138.75 651.25 1158.75 { lab=#net13}
N -100 1180 490 1200 { lab=#net5}
N 1610 1160 1610 1193.75 { lab=GND}
N 1610 1130 1663.75 1130 { lab=SUB}
N 1261.25 948.75 1262.5 915 { lab=vpwr}
N 1261.25 948.75 1261.25 978.75 { lab=vpwr}
N 1262.5 915 1391.25 968.75 { lab=vpwr}
N 1221.25 978.75 1351.25 998.75 { lab=#net54}
N 1391.25 968.75 1391.25 998.75 { lab=vpwr}
N 1262.5 915 1611.25 948.75 { lab=vpwr}
N 1611.25 948.75 1611.25 978.75 { lab=vpwr}
N 1570 1130 1571.25 978.75 { lab=#net55}
N 1391.25 1028.75 1471.25 1098.75 { lab=#net56}
N 1361.25 1088.75 1391.25 1028.75 { lab=#net56}
N 1262.5 915 1471.25 1128.75 { lab=vpwr}
N 1262.5 915 1361.25 1118.75 { lab=vpwr}
N 1321.25 1118.75 1570 1130 { lab=#net55}
N 1610 1100 1611.25 1008.75 { lab=#net57}
N 1431.25 1128.75 1610 1100 { lab=#net57}
N 1200 1130 1221.25 978.75 { lab=#net54}
N 1160 1180 1200 1190 { lab=#net5}
N 2200 1180 2200 1213.75 { lab=GND}
N 2200 1150 2253.75 1150 { lab=SUB}
N 1851.25 968.75 1852.5 935 { lab=vpwr}
N 1851.25 968.75 1851.25 998.75 { lab=vpwr}
N 1852.5 935 1981.25 988.75 { lab=vpwr}
N 1811.25 998.75 1941.25 1018.75 { lab=#net58}
N 1981.25 988.75 1981.25 1018.75 { lab=vpwr}
N 1852.5 935 2201.25 968.75 { lab=vpwr}
N 2201.25 968.75 2201.25 998.75 { lab=vpwr}
N 2160 1150 2161.25 998.75 { lab=#net59}
N 1981.25 1048.75 2061.25 1118.75 { lab=#net60}
N 1951.25 1108.75 1981.25 1048.75 { lab=#net60}
N 1852.5 935 2061.25 1148.75 { lab=vpwr}
N 1852.5 935 1951.25 1138.75 { lab=vpwr}
N 1911.25 1138.75 2160 1150 { lab=#net59}
N 2200 1120 2201.25 1028.75 { lab=#net61}
N 2021.25 1148.75 2200 1120 { lab=#net61}
N 1790 1150 1811.25 998.75 { lab=#net58}
N 1750 1200 1790 1210 { lab=#net5}
N 1060 1110 1100 1120 { lab=#net62}
N 1710 1090 1750 1100 { lab=#net63}
N 1750 1040 1811.25 998.75 { lab=#net58}
N 1100 1060 1221.25 978.75 { lab=#net54}
N 1471.25 1158.75 2061.25 1178.75 { lab=#net12}
N 1361.25 1148.75 1951.25 1168.75 { lab=#net13}
N 1200 1190 1790 1210 { lab=#net5}
N 651.25 1158.75 1361.25 1148.75 { lab=#net13}
N 761.25 1168.75 1471.25 1158.75 { lab=#net12}
N 490 1200 1200 1190 { lab=#net5}
N 81.25 648.75 101.25 158.75 { lab=#net13}
N 191.25 658.75 211.25 168.75 { lab=#net12}
N 61.25 1138.75 81.25 648.75 { lab=#net13}
N 171.25 1148.75 191.25 658.75 { lab=#net12}
N -80 690 -60 200 { lab=#net5}
N -100 1180 -80 690 { lab=#net5}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/nfet_01v8.sym} 330 140 0 0 {name=M5
L=0.15
W=0.42
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {lab_pin.sym} 350 203.75 0 0 {name=l1 sig_type=std_logic lab=GND}
C {lab_pin.sym} 403.75 140 2 0 {name=l2 sig_type=std_logic lab=SUB}
C {lab_pin.sym} 2.5 -75 1 0 {name=l14 sig_type=std_logic lab=vpwr}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} -18.75 -11.25 0 0 {name=M6
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 111.25 8.75 0 0 {name=M1
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 331.25 -11.25 0 0 {name=M2
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 81.25 128.75 0 0 {name=M3
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 191.25 138.75 0 0 {name=M4
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_var_lvt.sym} -60 170 0 0 {name=C2 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/nfet_01v8.sym} 920 160 0 0 {name=M7
L=0.15
W=0.42
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {lab_pin.sym} 940 223.75 0 0 {name=l3 sig_type=std_logic lab=GND}
C {lab_pin.sym} 993.75 160 2 0 {name=l4 sig_type=std_logic lab=SUB}
C {lab_pin.sym} 592.5 -55 1 0 {name=l5 sig_type=std_logic lab=vpwr}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 571.25 8.75 0 0 {name=M8
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 701.25 28.75 0 0 {name=M9
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 921.25 8.75 0 0 {name=M10
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 671.25 148.75 0 0 {name=M11
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 781.25 158.75 0 0 {name=M12
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_var_lvt.sym} 530 190 0 0 {name=C1 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_var_lvt.sym} -160 100 0 0 {name=C3 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_var_lvt.sym} 490 80 0 0 {name=C4 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/nfet_01v8.sym} 1630 150 0 0 {name=M13
L=0.15
W=0.42
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {lab_pin.sym} 1650 213.75 0 0 {name=l6 sig_type=std_logic lab=GND}
C {lab_pin.sym} 1703.75 150 2 0 {name=l7 sig_type=std_logic lab=SUB}
C {lab_pin.sym} 1302.5 -65 1 0 {name=l8 sig_type=std_logic lab=vpwr}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 1281.25 -1.25 0 0 {name=M14
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 1411.25 18.75 0 0 {name=M15
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 1631.25 -1.25 0 0 {name=M16
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 1381.25 138.75 0 0 {name=M17
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 1491.25 148.75 0 0 {name=M18
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_var_lvt.sym} 1240 180 0 0 {name=C5 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/nfet_01v8.sym} 2220 170 0 0 {name=M19
L=0.15
W=0.42
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {lab_pin.sym} 2240 233.75 0 0 {name=l9 sig_type=std_logic lab=GND}
C {lab_pin.sym} 2293.75 170 2 0 {name=l10 sig_type=std_logic lab=SUB}
C {lab_pin.sym} 1892.5 -45 1 0 {name=l11 sig_type=std_logic lab=vpwr}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 1871.25 18.75 0 0 {name=M20
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 2001.25 38.75 0 0 {name=M21
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 2221.25 18.75 0 0 {name=M22
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 1971.25 158.75 0 0 {name=M23
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 2081.25 168.75 0 0 {name=M24
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_var_lvt.sym} 1830 200 0 0 {name=C6 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_var_lvt.sym} 1140 110 0 0 {name=C7 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_var_lvt.sym} 1790 90 0 0 {name=C8 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/nfet_01v8.sym} 310 630 0 0 {name=M25
L=0.15
W=0.42
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {lab_pin.sym} 330 693.75 0 0 {name=l12 sig_type=std_logic lab=GND}
C {lab_pin.sym} 383.75 630 2 0 {name=l13 sig_type=std_logic lab=SUB}
C {lab_pin.sym} -17.5 415 1 0 {name=l15 sig_type=std_logic lab=vpwr}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} -38.75 478.75 0 0 {name=M26
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 91.25 498.75 0 0 {name=M27
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 311.25 478.75 0 0 {name=M28
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 61.25 618.75 0 0 {name=M29
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 171.25 628.75 0 0 {name=M30
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_var_lvt.sym} -80 660 0 0 {name=C9 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/nfet_01v8.sym} 900 650 0 0 {name=M31
L=0.15
W=0.42
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {lab_pin.sym} 920 713.75 0 0 {name=l16 sig_type=std_logic lab=GND}
C {lab_pin.sym} 973.75 650 2 0 {name=l17 sig_type=std_logic lab=SUB}
C {lab_pin.sym} 572.5 435 1 0 {name=l18 sig_type=std_logic lab=vpwr}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 551.25 498.75 0 0 {name=M32
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 681.25 518.75 0 0 {name=M33
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 901.25 498.75 0 0 {name=M34
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 651.25 638.75 0 0 {name=M35
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 761.25 648.75 0 0 {name=M36
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_var_lvt.sym} 510 680 0 0 {name=C10 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_var_lvt.sym} -180 590 0 0 {name=C11 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_var_lvt.sym} 470 570 0 0 {name=C12 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/nfet_01v8.sym} 1610 640 0 0 {name=M37
L=0.15
W=0.42
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {lab_pin.sym} 1630 703.75 0 0 {name=l19 sig_type=std_logic lab=GND}
C {lab_pin.sym} 1683.75 640 2 0 {name=l20 sig_type=std_logic lab=SUB}
C {lab_pin.sym} 1282.5 425 1 0 {name=l21 sig_type=std_logic lab=vpwr}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 1261.25 488.75 0 0 {name=M38
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 1391.25 508.75 0 0 {name=M39
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 1611.25 488.75 0 0 {name=M40
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 1361.25 628.75 0 0 {name=M41
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 1471.25 638.75 0 0 {name=M42
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_var_lvt.sym} 1220 670 0 0 {name=C13 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/nfet_01v8.sym} 2200 660 0 0 {name=M43
L=0.15
W=0.42
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {lab_pin.sym} 2220 723.75 0 0 {name=l22 sig_type=std_logic lab=GND}
C {lab_pin.sym} 2273.75 660 2 0 {name=l23 sig_type=std_logic lab=SUB}
C {lab_pin.sym} 1872.5 445 1 0 {name=l24 sig_type=std_logic lab=vpwr}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 1851.25 508.75 0 0 {name=M44
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 1981.25 528.75 0 0 {name=M45
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 2201.25 508.75 0 0 {name=M46
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 1951.25 648.75 0 0 {name=M47
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 2061.25 658.75 0 0 {name=M48
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_var_lvt.sym} 1810 690 0 0 {name=C14 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_var_lvt.sym} 1120 600 0 0 {name=C15 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_var_lvt.sym} 1770 580 0 0 {name=C16 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/nfet_01v8.sym} 290 1120 0 0 {name=M49
L=0.15
W=0.42
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {lab_pin.sym} 310 1183.75 0 0 {name=l25 sig_type=std_logic lab=GND}
C {lab_pin.sym} 363.75 1120 2 0 {name=l26 sig_type=std_logic lab=SUB}
C {lab_pin.sym} -37.5 905 1 0 {name=l27 sig_type=std_logic lab=vpwr}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} -58.75 968.75 0 0 {name=M50
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 71.25 988.75 0 0 {name=M51
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 291.25 968.75 0 0 {name=M52
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 41.25 1108.75 0 0 {name=M53
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 151.25 1118.75 0 0 {name=M54
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_var_lvt.sym} -100 1150 0 0 {name=C17 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/nfet_01v8.sym} 880 1140 0 0 {name=M55
L=0.15
W=0.42
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {lab_pin.sym} 900 1203.75 0 0 {name=l28 sig_type=std_logic lab=GND}
C {lab_pin.sym} 953.75 1140 2 0 {name=l29 sig_type=std_logic lab=SUB}
C {lab_pin.sym} 552.5 925 1 0 {name=l30 sig_type=std_logic lab=vpwr}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 531.25 988.75 0 0 {name=M56
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 661.25 1008.75 0 0 {name=M57
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 881.25 988.75 0 0 {name=M58
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 631.25 1128.75 0 0 {name=M59
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 741.25 1138.75 0 0 {name=M60
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_var_lvt.sym} 490 1170 0 0 {name=C18 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_var_lvt.sym} -200 1080 0 0 {name=C19 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_var_lvt.sym} 450 1060 0 0 {name=C20 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/nfet_01v8.sym} 1590 1130 0 0 {name=M61
L=0.15
W=0.42
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {lab_pin.sym} 1610 1193.75 0 0 {name=l31 sig_type=std_logic lab=GND}
C {lab_pin.sym} 1663.75 1130 2 0 {name=l32 sig_type=std_logic lab=SUB}
C {lab_pin.sym} 1262.5 915 1 0 {name=l33 sig_type=std_logic lab=vpwr}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 1241.25 978.75 0 0 {name=M62
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 1371.25 998.75 0 0 {name=M63
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 1591.25 978.75 0 0 {name=M64
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 1341.25 1118.75 0 0 {name=M65
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 1451.25 1128.75 0 0 {name=M66
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_var_lvt.sym} 1200 1160 0 0 {name=C21 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/nfet_01v8.sym} 2180 1150 0 0 {name=M67
L=0.15
W=0.42
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {lab_pin.sym} 2200 1213.75 0 0 {name=l34 sig_type=std_logic lab=GND}
C {lab_pin.sym} 2253.75 1150 2 0 {name=l35 sig_type=std_logic lab=SUB}
C {lab_pin.sym} 1852.5 935 1 0 {name=l36 sig_type=std_logic lab=vpwr}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 1831.25 998.75 0 0 {name=M68
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 1961.25 1018.75 0 0 {name=M69
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 2181.25 998.75 0 0 {name=M70
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 1931.25 1138.75 0 0 {name=M71
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 2041.25 1148.75 0 0 {name=M72
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_var_lvt.sym} 1790 1180 0 0 {name=C22 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_var_lvt.sym} 1100 1090 0 0 {name=C23 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_var_lvt.sym} 1750 1070 0 0 {name=C24 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
