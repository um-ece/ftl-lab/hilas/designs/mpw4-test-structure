v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 340 290 340 323.75 { lab=GND}
N 340 260 393.75 260 { lab=SUB}
N -78.75 -1.25 -78.75 28.75 { lab=vpwr}
N 311.25 8.75 311.25 38.75 { lab=vpwr}
N 180 300 180 333.75 { lab=GND}
N 180 270 233.75 270 { lab=SUB}
N 30 300 30 333.75 { lab=GND}
N 30 270 83.75 270 { lab=SUB}
N -120 280 -120 313.75 { lab=GND}
N -120 250 -66.25 250 { lab=SUB}
N -160 250 -10 270 { lab=#net1}
N 140 270 300 260 { lab=#net2}
N 171.25 138.75 180 240 { lab=#net2}
N 30 240 51.25 138.75 { lab=#net1}
N 140 270 180 240 { lab=#net2}
N -10 270 30 240 { lab=#net1}
N 111.25 -51.25 111.25 -21.25 { lab=vpwr}
N 2.5 -75 111.25 -51.25 { lab=vpwr}
N 2.5 -75 171.25 108.75 { lab=vpwr}
N 2.5 -75 51.25 108.75 { lab=vpwr}
N 51.25 78.75 111.25 8.75 { lab=#net3}
N 111.25 8.75 171.25 78.75 { lab=#net3}
N -78.75 -1.25 2.5 -75 { lab=vpwr}
N 2.5 -75 311.25 8.75 { lab=vpwr}
N -120 220 -78.75 58.75 { lab=#net4}
N -118.75 28.75 -78.75 58.75 { lab=#net4}
N 340 185 393.75 185 { lab=SUB}
N 311.25 68.75 341.25 78.75 { lab=#net5}
N 340 155 341.25 138.75 { lab=#net6}
N 340 215 340 230 { lab=#net7}
N -78.75 58.75 271.25 38.75 { lab=#net4}
N 2.5 -75 341.25 108.75 { lab=vpwr}
N -305 280 -305 313.75 { lab=GND}
N -305 250 -251.25 250 { lab=SUB}
N -318.75 53.75 -318.75 83.75 { lab=vpwr}
N -490 280 -490 313.75 { lab=GND}
N -490 250 -436.25 250 { lab=SUB}
N -503.75 53.75 -503.75 83.75 { lab=vpwr}
N -503.75 53.75 2.5 -75 { lab=vpwr}
N -318.75 53.75 2.5 -75 { lab=vpwr}
N -503.75 113.75 -490 220 { lab=#net8}
N -318.75 113.75 -305 220 { lab=#net9}
N -530 250 -490 220 { lab=#net8}
N -358.75 83.75 -318.75 113.75 { lab=#net9}
N -318.75 113.75 301.25 108.75 { lab=#net9}
N -490 220 300 185 { lab=#net8}
N -380 300 -345 250 {}
N -590 90 -543.75 83.75 {}
N -543.75 83.75 71.25 -21.25 {}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/nfet_01v8.sym} 320 260 0 0 {name=M5
L=0.15
W=1.5
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {lab_pin.sym} 340 323.75 0 0 {name=l1 sig_type=std_logic lab=GND}
C {lab_pin.sym} 393.75 260 2 0 {name=l2 sig_type=std_logic lab=SUB}
C {lab_pin.sym} 2.5 -75 1 0 {name=l14 sig_type=std_logic lab=vpwr}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} -98.75 28.75 0 0 {name=M6
L=0.15
W=1.5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 91.25 -21.25 0 0 {name=M1
L=0.15
W=20
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 291.25 38.75 0 0 {name=M2
L=0.15
W=1.5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 31.25 108.75 0 0 {name=M3
L=0.15
W=20
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 151.25 108.75 0 0 {name=M4
L=0.15
W=20
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/nfet_01v8.sym} 160 270 0 0 {name=M7
L=0.15
W=1.5
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {lab_pin.sym} 180 333.75 0 0 {name=l3 sig_type=std_logic lab=GND}
C {lab_pin.sym} 233.75 270 2 0 {name=l4 sig_type=std_logic lab=SUB}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/nfet_01v8.sym} 10 270 0 0 {name=M8
L=0.15
W=1.5
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {lab_pin.sym} 30 333.75 0 0 {name=l5 sig_type=std_logic lab=GND}
C {lab_pin.sym} 83.75 270 2 0 {name=l6 sig_type=std_logic lab=SUB}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/nfet_01v8.sym} -140 250 0 0 {name=M9
L=0.15
W=1.5
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {lab_pin.sym} -120 313.75 0 0 {name=l7 sig_type=std_logic lab=GND}
C {lab_pin.sym} -66.25 250 2 0 {name=l8 sig_type=std_logic lab=SUB}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/nfet_01v8.sym} 320 185 0 0 {name=M10
L=0.4
W=20
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {lab_pin.sym} 393.75 185 2 0 {name=l10 sig_type=std_logic lab=SUB}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 321.25 108.75 0 0 {name=M11
L=0.4
W=20
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/nfet_01v8.sym} -325 250 0 0 {name=M12
L=0.2
W=2
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {lab_pin.sym} -305 313.75 0 0 {name=l9 sig_type=std_logic lab=GND}
C {lab_pin.sym} -251.25 250 2 0 {name=l11 sig_type=std_logic lab=SUB}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} -338.75 83.75 0 0 {name=M13
L=1.05
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/nfet_01v8.sym} -510 250 0 0 {name=M14
L=2
W=0.42
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {lab_pin.sym} -490 313.75 0 0 {name=l12 sig_type=std_logic lab=GND}
C {lab_pin.sym} -436.25 250 2 0 {name=l13 sig_type=std_logic lab=SUB}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} -523.75 83.75 0 0 {name=M15
L=0.2
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {lab_pin.sym} -590 90 0 0 {name=l15 sig_type=std_logic lab=pbias}
C {lab_pin.sym} -380 300 0 0 {name=l16 sig_type=std_logic lab=nbias}
