v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 340 290 340 323.75 { lab=GND}
N 340 260 393.75 260 { lab=SUB}
N -78.75 -1.25 -78.75 28.75 { lab=vpwr}
N 311.25 8.75 311.25 38.75 { lab=vpwr}
N 180 300 180 333.75 { lab=GND}
N 180 270 233.75 270 { lab=SUB}
N 30 300 30 333.75 { lab=GND}
N 30 270 83.75 270 { lab=SUB}
N -120 280 -120 313.75 { lab=GND}
N -120 250 -66.25 250 { lab=SUB}
N -160 250 -10 270 { lab=#net1}
N 140 270 300 260 { lab=#net2}
N 171.25 138.75 180 240 { lab=#net2}
N 30 240 51.25 138.75 { lab=#net1}
N 140 270 180 240 { lab=#net2}
N -10 270 30 240 { lab=#net1}
N 111.25 -51.25 111.25 -21.25 { lab=vpwr}
N 2.5 -75 111.25 -51.25 { lab=vpwr}
N 2.5 -75 171.25 108.75 { lab=vpwr}
N 2.5 -75 51.25 108.75 { lab=vpwr}
N 51.25 78.75 111.25 8.75 { lab=#net3}
N 111.25 8.75 171.25 78.75 { lab=#net3}
N -78.75 -1.25 2.5 -75 { lab=vpwr}
N 2.5 -75 311.25 8.75 { lab=vpwr}
N -120 220 -78.75 58.75 { lab=#net4}
N -118.75 28.75 -78.75 58.75 { lab=#net4}
N 340 185 393.75 185 { lab=SUB}
N 311.25 68.75 341.25 78.75 { lab=#net5}
N 340 155 341.25 138.75 { lab=#net6}
N 340 215 340 230 { lab=#net7}
N -78.75 58.75 271.25 38.75 { lab=#net4}
N 2.5 -75 341.25 108.75 { lab=vpwr}
N -305 280 -305 313.75 { lab=GND}
N -305 250 -251.25 250 { lab=SUB}
N -318.75 53.75 -318.75 83.75 { lab=vpwr}
N -490 280 -490 313.75 { lab=GND}
N -490 250 -436.25 250 { lab=SUB}
N -503.75 53.75 -503.75 83.75 { lab=vpwr}
N -503.75 53.75 2.5 -75 { lab=vpwr}
N -318.75 53.75 2.5 -75 { lab=vpwr}
N -503.75 113.75 -490 220 { lab=#net8}
N -318.75 113.75 -305 220 { lab=#net9}
N -530 250 -490 220 { lab=#net8}
N -358.75 83.75 -318.75 113.75 { lab=#net9}
N -318.75 113.75 301.25 108.75 { lab=#net9}
N -490 220 300 185 { lab=#net8}
N -380 300 -345 250 {}
N -590 90 -543.75 83.75 {}
N 230 -300 230 -266.25 { lab=GND}
N 230 -330 283.75 -330 { lab=SUB}
N -188.75 -591.25 -188.75 -561.25 { lab=vpwr}
N 201.25 -581.25 201.25 -551.25 { lab=vpwr}
N 70 -290 70 -256.25 { lab=GND}
N 70 -320 123.75 -320 { lab=SUB}
N -80 -290 -80 -256.25 { lab=GND}
N -80 -320 -26.25 -320 { lab=SUB}
N -230 -310 -230 -276.25 { lab=GND}
N -230 -340 -176.25 -340 { lab=SUB}
N -270 -340 -120 -320 { lab=#net1}
N 30 -320 190 -330 { lab=#net2}
N 61.25 -451.25 70 -350 { lab=#net2}
N -80 -350 -58.75 -451.25 { lab=#net1}
N 30 -320 70 -350 { lab=#net2}
N -120 -320 -80 -350 { lab=#net1}
N 1.25 -641.25 1.25 -611.25 { lab=vpwr}
N -107.5 -665 1.25 -641.25 { lab=vpwr}
N -107.5 -665 61.25 -481.25 { lab=vpwr}
N -107.5 -665 -58.75 -481.25 { lab=vpwr}
N -58.75 -511.25 1.25 -581.25 { lab=#net3}
N 1.25 -581.25 61.25 -511.25 { lab=#net3}
N -188.75 -591.25 -107.5 -665 { lab=vpwr}
N -107.5 -665 201.25 -581.25 { lab=vpwr}
N -230 -370 -188.75 -531.25 { lab=#net4}
N -228.75 -561.25 -188.75 -531.25 { lab=#net4}
N -188.75 -531.25 161.25 -551.25 { lab=#net4}
N 201.25 -521.25 230 -360 { lab=#net5}
N -415 -325 -375 -315 { lab=#net6}
N -415 -430 -375 -420 { lab=#net7}
N -375 -480 -38.75 -611.25 { lab=#net8}
N -375 -375 -38.75 -611.25 { lab=#net8}
N 1130 -630 1130 -596.25 { lab=GND}
N 1130 -660 1183.75 -660 { lab=SUB}
N 781.25 -841.25 782.5 -875 { lab=vpwr}
N 781.25 -841.25 781.25 -811.25 { lab=vpwr}
N 782.5 -875 911.25 -821.25 { lab=vpwr}
N 741.25 -811.25 871.25 -791.25 { lab=#net1}
N 911.25 -821.25 911.25 -791.25 { lab=vpwr}
N 782.5 -875 1131.25 -841.25 { lab=vpwr}
N 1131.25 -841.25 1131.25 -811.25 { lab=vpwr}
N 1090 -660 1091.25 -811.25 { lab=#net2}
N 911.25 -761.25 991.25 -691.25 { lab=#net3}
N 881.25 -701.25 911.25 -761.25 { lab=#net3}
N 782.5 -875 991.25 -661.25 { lab=vpwr}
N 782.5 -875 881.25 -671.25 { lab=vpwr}
N 841.25 -671.25 1090 -660 { lab=#net2}
N 1130 -690 1131.25 -781.25 { lab=#net4}
N 951.25 -661.25 1130 -690 { lab=#net4}
N 720 -660 741.25 -811.25 { lab=#net1}
N 680 -610 720 -600 { lab=#net5}
N 1720 -610 1720 -576.25 { lab=GND}
N 1720 -640 1773.75 -640 { lab=SUB}
N 1371.25 -821.25 1372.5 -855 { lab=vpwr}
N 1371.25 -821.25 1371.25 -791.25 { lab=vpwr}
N 1372.5 -855 1501.25 -801.25 { lab=vpwr}
N 1331.25 -791.25 1461.25 -771.25 { lab=#net6}
N 1501.25 -801.25 1501.25 -771.25 { lab=vpwr}
N 1372.5 -855 1721.25 -821.25 { lab=vpwr}
N 1721.25 -821.25 1721.25 -791.25 { lab=vpwr}
N 1680 -640 1681.25 -791.25 { lab=#net7}
N 1501.25 -741.25 1581.25 -671.25 { lab=#net8}
N 1471.25 -681.25 1501.25 -741.25 { lab=#net8}
N 1372.5 -855 1581.25 -641.25 { lab=vpwr}
N 1372.5 -855 1471.25 -651.25 { lab=vpwr}
N 1431.25 -651.25 1680 -640 { lab=#net7}
N 1720 -670 1721.25 -761.25 { lab=#net9}
N 1541.25 -641.25 1720 -670 { lab=#net9}
N 1310 -640 1331.25 -791.25 { lab=#net6}
N 1270 -590 1310 -580 { lab=#net5}
N 580 -680 620 -670 { lab=#net10}
N 1230 -700 1270 -690 { lab=#net11}
N 1270 -750 1331.25 -791.25 { lab=#net6}
N 620 -730 741.25 -811.25 { lab=#net1}
N 991.25 -631.25 1581.25 -611.25 { lab=#net12}
N 881.25 -641.25 1471.25 -621.25 { lab=#net13}
N 720 -600 1310 -580 { lab=#net5}
N 2430 -620 2430 -586.25 { lab=GND}
N 2430 -650 2483.75 -650 { lab=SUB}
N 2081.25 -831.25 2082.5 -865 { lab=vpwr}
N 2081.25 -831.25 2081.25 -801.25 { lab=vpwr}
N 2082.5 -865 2211.25 -811.25 { lab=vpwr}
N 2041.25 -801.25 2171.25 -781.25 { lab=#net14}
N 2211.25 -811.25 2211.25 -781.25 { lab=vpwr}
N 2082.5 -865 2431.25 -831.25 { lab=vpwr}
N 2431.25 -831.25 2431.25 -801.25 { lab=vpwr}
N 2390 -650 2391.25 -801.25 { lab=#net15}
N 2211.25 -751.25 2291.25 -681.25 { lab=#net16}
N 2181.25 -691.25 2211.25 -751.25 { lab=#net16}
N 2082.5 -865 2291.25 -651.25 { lab=vpwr}
N 2082.5 -865 2181.25 -661.25 { lab=vpwr}
N 2141.25 -661.25 2390 -650 { lab=#net15}
N 2430 -680 2431.25 -771.25 { lab=#net17}
N 2251.25 -651.25 2430 -680 { lab=#net17}
N 2020 -650 2041.25 -801.25 { lab=#net14}
N 1980 -600 2020 -590 { lab=#net5}
N 3020 -600 3020 -566.25 { lab=GND}
N 3020 -630 3073.75 -630 { lab=SUB}
N 2671.25 -811.25 2672.5 -845 { lab=vpwr}
N 2671.25 -811.25 2671.25 -781.25 { lab=vpwr}
N 2672.5 -845 2801.25 -791.25 { lab=vpwr}
N 2631.25 -781.25 2761.25 -761.25 { lab=#net18}
N 2801.25 -791.25 2801.25 -761.25 { lab=vpwr}
N 2672.5 -845 3021.25 -811.25 { lab=vpwr}
N 3021.25 -811.25 3021.25 -781.25 { lab=vpwr}
N 2980 -630 2981.25 -781.25 { lab=#net19}
N 2801.25 -731.25 2881.25 -661.25 { lab=#net20}
N 2771.25 -671.25 2801.25 -731.25 { lab=#net20}
N 2672.5 -845 2881.25 -631.25 { lab=vpwr}
N 2672.5 -845 2771.25 -641.25 { lab=vpwr}
N 2731.25 -641.25 2980 -630 { lab=#net19}
N 3020 -660 3021.25 -751.25 { lab=#net21}
N 2841.25 -631.25 3020 -660 { lab=#net21}
N 2610 -630 2631.25 -781.25 { lab=#net18}
N 2570 -580 2610 -570 { lab=#net5}
N 1880 -670 1920 -660 { lab=#net22}
N 2530 -690 2570 -680 { lab=#net23}
N 2570 -740 2631.25 -781.25 { lab=#net18}
N 1920 -720 2041.25 -801.25 { lab=#net14}
N 2291.25 -621.25 2881.25 -601.25 { lab=#net12}
N 2181.25 -631.25 2771.25 -611.25 { lab=#net13}
N 2020 -590 2610 -570 { lab=#net5}
N 1471.25 -621.25 2181.25 -631.25 { lab=#net13}
N 1581.25 -611.25 2291.25 -621.25 { lab=#net12}
N 1310 -580 2020 -590 { lab=#net5}
N 1110 -140 1110 -106.25 { lab=GND}
N 1110 -170 1163.75 -170 { lab=SUB}
N 761.25 -351.25 762.5 -385 { lab=vpwr}
N 761.25 -351.25 761.25 -321.25 { lab=vpwr}
N 762.5 -385 891.25 -331.25 { lab=vpwr}
N 721.25 -321.25 851.25 -301.25 { lab=#net24}
N 891.25 -331.25 891.25 -301.25 { lab=vpwr}
N 762.5 -385 1111.25 -351.25 { lab=vpwr}
N 1111.25 -351.25 1111.25 -321.25 { lab=vpwr}
N 1070 -170 1071.25 -321.25 { lab=#net25}
N 891.25 -271.25 971.25 -201.25 { lab=#net26}
N 861.25 -211.25 891.25 -271.25 { lab=#net26}
N 762.5 -385 971.25 -171.25 { lab=vpwr}
N 762.5 -385 861.25 -181.25 { lab=vpwr}
N 821.25 -181.25 1070 -170 { lab=#net25}
N 1110 -200 1111.25 -291.25 { lab=#net27}
N 931.25 -171.25 1110 -200 { lab=#net27}
N 700 -170 721.25 -321.25 { lab=#net24}
N 660 -120 700 -110 { lab=#net5}
N 1700 -120 1700 -86.25 { lab=GND}
N 1700 -150 1753.75 -150 { lab=SUB}
N 1351.25 -331.25 1352.5 -365 { lab=vpwr}
N 1351.25 -331.25 1351.25 -301.25 { lab=vpwr}
N 1352.5 -365 1481.25 -311.25 { lab=vpwr}
N 1311.25 -301.25 1441.25 -281.25 { lab=#net28}
N 1481.25 -311.25 1481.25 -281.25 { lab=vpwr}
N 1352.5 -365 1701.25 -331.25 { lab=vpwr}
N 1701.25 -331.25 1701.25 -301.25 { lab=vpwr}
N 1660 -150 1661.25 -301.25 { lab=#net29}
N 1481.25 -251.25 1561.25 -181.25 { lab=#net30}
N 1451.25 -191.25 1481.25 -251.25 { lab=#net30}
N 1352.5 -365 1561.25 -151.25 { lab=vpwr}
N 1352.5 -365 1451.25 -161.25 { lab=vpwr}
N 1411.25 -161.25 1660 -150 { lab=#net29}
N 1700 -180 1701.25 -271.25 { lab=#net31}
N 1521.25 -151.25 1700 -180 { lab=#net31}
N 1290 -150 1311.25 -301.25 { lab=#net28}
N 1250 -100 1290 -90 { lab=#net5}
N 560 -190 600 -180 { lab=#net32}
N 1210 -210 1250 -200 { lab=#net33}
N 1250 -260 1311.25 -301.25 { lab=#net28}
N 600 -240 721.25 -321.25 { lab=#net24}
N 971.25 -141.25 1561.25 -121.25 { lab=#net12}
N 861.25 -151.25 1451.25 -131.25 { lab=#net13}
N 700 -110 1290 -90 { lab=#net5}
N 2410 -130 2410 -96.25 { lab=GND}
N 2410 -160 2463.75 -160 { lab=SUB}
N 2061.25 -341.25 2062.5 -375 { lab=vpwr}
N 2061.25 -341.25 2061.25 -311.25 { lab=vpwr}
N 2062.5 -375 2191.25 -321.25 { lab=vpwr}
N 2021.25 -311.25 2151.25 -291.25 { lab=#net34}
N 2191.25 -321.25 2191.25 -291.25 { lab=vpwr}
N 2062.5 -375 2411.25 -341.25 { lab=vpwr}
N 2411.25 -341.25 2411.25 -311.25 { lab=vpwr}
N 2370 -160 2371.25 -311.25 { lab=#net35}
N 2191.25 -261.25 2271.25 -191.25 { lab=#net36}
N 2161.25 -201.25 2191.25 -261.25 { lab=#net36}
N 2062.5 -375 2271.25 -161.25 { lab=vpwr}
N 2062.5 -375 2161.25 -171.25 { lab=vpwr}
N 2121.25 -171.25 2370 -160 { lab=#net35}
N 2410 -190 2411.25 -281.25 { lab=#net37}
N 2231.25 -161.25 2410 -190 { lab=#net37}
N 2000 -160 2021.25 -311.25 { lab=#net34}
N 1960 -110 2000 -100 { lab=#net5}
N 3000 -110 3000 -76.25 { lab=GND}
N 3000 -140 3053.75 -140 { lab=SUB}
N 2651.25 -321.25 2652.5 -355 { lab=vpwr}
N 2651.25 -321.25 2651.25 -291.25 { lab=vpwr}
N 2652.5 -355 2781.25 -301.25 { lab=vpwr}
N 2611.25 -291.25 2741.25 -271.25 { lab=#net38}
N 2781.25 -301.25 2781.25 -271.25 { lab=vpwr}
N 2652.5 -355 3001.25 -321.25 { lab=vpwr}
N 3001.25 -321.25 3001.25 -291.25 { lab=vpwr}
N 2960 -140 2961.25 -291.25 { lab=#net39}
N 2781.25 -241.25 2861.25 -171.25 { lab=#net40}
N 2751.25 -181.25 2781.25 -241.25 { lab=#net40}
N 2652.5 -355 2861.25 -141.25 { lab=vpwr}
N 2652.5 -355 2751.25 -151.25 { lab=vpwr}
N 2711.25 -151.25 2960 -140 { lab=#net39}
N 3000 -170 3001.25 -261.25 { lab=#net41}
N 2821.25 -141.25 3000 -170 { lab=#net41}
N 2590 -140 2611.25 -291.25 { lab=#net38}
N 2550 -90 2590 -80 { lab=#net5}
N 1860 -180 1900 -170 { lab=#net42}
N 2510 -200 2550 -190 { lab=#net43}
N 2550 -250 2611.25 -291.25 { lab=#net38}
N 1900 -230 2021.25 -311.25 { lab=#net34}
N 2271.25 -131.25 2861.25 -111.25 { lab=#net12}
N 2161.25 -141.25 2751.25 -121.25 { lab=#net13}
N 2000 -100 2590 -80 { lab=#net5}
N 1451.25 -131.25 2161.25 -141.25 { lab=#net13}
N 1561.25 -121.25 2271.25 -131.25 { lab=#net12}
N 1290 -90 2000 -100 { lab=#net5}
N 1090 350 1090 383.75 { lab=GND}
N 1090 320 1143.75 320 { lab=SUB}
N 741.25 138.75 742.5 105 { lab=vpwr}
N 741.25 138.75 741.25 168.75 { lab=vpwr}
N 742.5 105 871.25 158.75 { lab=vpwr}
N 701.25 168.75 831.25 188.75 { lab=#net44}
N 871.25 158.75 871.25 188.75 { lab=vpwr}
N 742.5 105 1091.25 138.75 { lab=vpwr}
N 1091.25 138.75 1091.25 168.75 { lab=vpwr}
N 1050 320 1051.25 168.75 { lab=#net45}
N 871.25 218.75 951.25 288.75 { lab=#net46}
N 841.25 278.75 871.25 218.75 { lab=#net46}
N 742.5 105 951.25 318.75 { lab=vpwr}
N 742.5 105 841.25 308.75 { lab=vpwr}
N 801.25 308.75 1050 320 { lab=#net45}
N 1090 290 1091.25 198.75 { lab=#net47}
N 911.25 318.75 1090 290 { lab=#net47}
N 680 320 701.25 168.75 { lab=#net44}
N 640 370 680 380 { lab=#net5}
N 1680 370 1680 403.75 { lab=GND}
N 1680 340 1733.75 340 { lab=SUB}
N 1331.25 158.75 1332.5 125 { lab=vpwr}
N 1331.25 158.75 1331.25 188.75 { lab=vpwr}
N 1332.5 125 1461.25 178.75 { lab=vpwr}
N 1291.25 188.75 1421.25 208.75 { lab=#net48}
N 1461.25 178.75 1461.25 208.75 { lab=vpwr}
N 1332.5 125 1681.25 158.75 { lab=vpwr}
N 1681.25 158.75 1681.25 188.75 { lab=vpwr}
N 1640 340 1641.25 188.75 { lab=#net49}
N 1461.25 238.75 1541.25 308.75 { lab=#net50}
N 1431.25 298.75 1461.25 238.75 { lab=#net50}
N 1332.5 125 1541.25 338.75 { lab=vpwr}
N 1332.5 125 1431.25 328.75 { lab=vpwr}
N 1391.25 328.75 1640 340 { lab=#net49}
N 1680 310 1681.25 218.75 { lab=#net51}
N 1501.25 338.75 1680 310 { lab=#net51}
N 1270 340 1291.25 188.75 { lab=#net48}
N 1230 390 1270 400 { lab=#net5}
N 540 300 580 310 { lab=#net52}
N 1190 280 1230 290 { lab=#net53}
N 1230 230 1291.25 188.75 { lab=#net48}
N 580 250 701.25 168.75 { lab=#net44}
N 951.25 348.75 1541.25 368.75 { lab=#net12}
N 841.25 338.75 1431.25 358.75 { lab=#net13}
N 680 380 1270 400 { lab=#net5}
N 2390 360 2390 393.75 { lab=GND}
N 2390 330 2443.75 330 { lab=SUB}
N 2041.25 148.75 2042.5 115 { lab=vpwr}
N 2041.25 148.75 2041.25 178.75 { lab=vpwr}
N 2042.5 115 2171.25 168.75 { lab=vpwr}
N 2001.25 178.75 2131.25 198.75 { lab=#net54}
N 2171.25 168.75 2171.25 198.75 { lab=vpwr}
N 2042.5 115 2391.25 148.75 { lab=vpwr}
N 2391.25 148.75 2391.25 178.75 { lab=vpwr}
N 2350 330 2351.25 178.75 { lab=#net55}
N 2171.25 228.75 2251.25 298.75 { lab=#net56}
N 2141.25 288.75 2171.25 228.75 { lab=#net56}
N 2042.5 115 2251.25 328.75 { lab=vpwr}
N 2042.5 115 2141.25 318.75 { lab=vpwr}
N 2101.25 318.75 2350 330 { lab=#net55}
N 2390 300 2391.25 208.75 { lab=#net57}
N 2211.25 328.75 2390 300 { lab=#net57}
N 1980 330 2001.25 178.75 { lab=#net54}
N 1940 380 1980 390 { lab=#net5}
N 2980 380 2980 413.75 { lab=GND}
N 2980 350 3033.75 350 { lab=SUB}
N 2631.25 168.75 2632.5 135 { lab=vpwr}
N 2631.25 168.75 2631.25 198.75 { lab=vpwr}
N 2632.5 135 2761.25 188.75 { lab=vpwr}
N 2591.25 198.75 2721.25 218.75 { lab=#net58}
N 2761.25 188.75 2761.25 218.75 { lab=vpwr}
N 2632.5 135 2981.25 168.75 { lab=vpwr}
N 2981.25 168.75 2981.25 198.75 { lab=vpwr}
N 2940 350 2941.25 198.75 { lab=#net59}
N 2761.25 248.75 2841.25 318.75 { lab=#net60}
N 2731.25 308.75 2761.25 248.75 { lab=#net60}
N 2632.5 135 2841.25 348.75 { lab=vpwr}
N 2632.5 135 2731.25 338.75 { lab=vpwr}
N 2691.25 338.75 2940 350 { lab=#net59}
N 2980 320 2981.25 228.75 { lab=#net61}
N 2801.25 348.75 2980 320 { lab=#net61}
N 2570 350 2591.25 198.75 { lab=#net58}
N 2530 400 2570 410 { lab=#net5}
N 1840 310 1880 320 { lab=#net62}
N 2490 290 2530 300 { lab=#net63}
N 2530 240 2591.25 198.75 { lab=#net58}
N 1880 260 2001.25 178.75 { lab=#net54}
N 2251.25 358.75 2841.25 378.75 { lab=#net12}
N 2141.25 348.75 2731.25 368.75 { lab=#net13}
N 1980 390 2570 410 { lab=#net5}
N 1431.25 358.75 2141.25 348.75 { lab=#net13}
N 1541.25 368.75 2251.25 358.75 { lab=#net12}
N 1270 400 1980 390 { lab=#net5}
N 861.25 -151.25 881.25 -641.25 { lab=#net13}
N 971.25 -141.25 991.25 -631.25 { lab=#net12}
N 841.25 338.75 861.25 -151.25 { lab=#net13}
N 951.25 348.75 971.25 -141.25 { lab=#net12}
N 700 -110 720 -600 { lab=#net5}
N 680 380 700 -110 { lab=#net5}
N 11.25 108.75 841.25 338.75 {}
N 131.25 108.75 951.25 348.75 {}
N -98.75 -481.25 230 -360 {}
N 11.25 108.75 230 -360 {}
N -543.75 83.75 71.25 -21.25 {}
N 21.25 -481.25 340 155 {}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/nfet_01v8.sym} 320 260 0 0 {name=M5
L=0.15
W=1.5
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {lab_pin.sym} 340 323.75 0 0 {name=l1 sig_type=std_logic lab=GND}
C {lab_pin.sym} 393.75 260 2 0 {name=l2 sig_type=std_logic lab=SUB}
C {lab_pin.sym} 2.5 -75 1 0 {name=l14 sig_type=std_logic lab=vpwr}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} -98.75 28.75 0 0 {name=M6
L=0.15
W=1.5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 91.25 -21.25 0 0 {name=M1
L=0.15
W=20
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 291.25 38.75 0 0 {name=M2
L=0.15
W=1.5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 31.25 108.75 0 0 {name=M3
L=0.15
W=20
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 151.25 108.75 0 0 {name=M4
L=0.15
W=20
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/nfet_01v8.sym} 160 270 0 0 {name=M7
L=0.15
W=1.5
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {lab_pin.sym} 180 333.75 0 0 {name=l3 sig_type=std_logic lab=GND}
C {lab_pin.sym} 233.75 270 2 0 {name=l4 sig_type=std_logic lab=SUB}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/nfet_01v8.sym} 10 270 0 0 {name=M8
L=0.15
W=1.5
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {lab_pin.sym} 30 333.75 0 0 {name=l5 sig_type=std_logic lab=GND}
C {lab_pin.sym} 83.75 270 2 0 {name=l6 sig_type=std_logic lab=SUB}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/nfet_01v8.sym} -140 250 0 0 {name=M9
L=0.15
W=1.5
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {lab_pin.sym} -120 313.75 0 0 {name=l7 sig_type=std_logic lab=GND}
C {lab_pin.sym} -66.25 250 2 0 {name=l8 sig_type=std_logic lab=SUB}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/nfet_01v8.sym} 320 185 0 0 {name=M10
L=0.4
W=20
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {lab_pin.sym} 393.75 185 2 0 {name=l10 sig_type=std_logic lab=SUB}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 321.25 108.75 0 0 {name=M11
L=0.4
W=20
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/nfet_01v8.sym} -325 250 0 0 {name=M12
L=0.2
W=2
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {lab_pin.sym} -305 313.75 0 0 {name=l9 sig_type=std_logic lab=GND}
C {lab_pin.sym} -251.25 250 2 0 {name=l11 sig_type=std_logic lab=SUB}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} -338.75 83.75 0 0 {name=M13
L=1.05
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/nfet_01v8.sym} -510 250 0 0 {name=M14
L=2
W=0.42
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {lab_pin.sym} -490 313.75 0 0 {name=l12 sig_type=std_logic lab=GND}
C {lab_pin.sym} -436.25 250 2 0 {name=l13 sig_type=std_logic lab=SUB}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} -523.75 83.75 0 0 {name=M15
L=0.2
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {lab_pin.sym} -590 90 0 0 {name=l15 sig_type=std_logic lab=pbias}
C {lab_pin.sym} -380 300 0 0 {name=l16 sig_type=std_logic lab=nbias}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/nfet_01v8.sym} 210 -330 0 0 {name=M16
L=0.15
W=1.5
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {lab_pin.sym} 230 -266.25 0 0 {name=l17 sig_type=std_logic lab=GND}
C {lab_pin.sym} 283.75 -330 2 0 {name=l18 sig_type=std_logic lab=SUB}
C {lab_pin.sym} -107.5 -665 1 0 {name=l19 sig_type=std_logic lab=vpwr}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} -208.75 -561.25 0 0 {name=M17
L=0.15
W=1.5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} -18.75 -611.25 0 0 {name=M18
L=0.15
W=20
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 181.25 -551.25 0 0 {name=M19
L=0.15
W=1.5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} -78.75 -481.25 0 0 {name=M20
L=0.15
W=20
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 41.25 -481.25 0 0 {name=M21
L=0.15
W=20
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/nfet_01v8.sym} 50 -320 0 0 {name=M22
L=0.15
W=1.5
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {lab_pin.sym} 70 -256.25 0 0 {name=l20 sig_type=std_logic lab=GND}
C {lab_pin.sym} 123.75 -320 2 0 {name=l21 sig_type=std_logic lab=SUB}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/nfet_01v8.sym} -100 -320 0 0 {name=M23
L=0.15
W=1.5
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {lab_pin.sym} -80 -256.25 0 0 {name=l22 sig_type=std_logic lab=GND}
C {lab_pin.sym} -26.25 -320 2 0 {name=l23 sig_type=std_logic lab=SUB}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/nfet_01v8.sym} -250 -340 0 0 {name=M24
L=0.15
W=1.5
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {lab_pin.sym} -230 -276.25 0 0 {name=l24 sig_type=std_logic lab=GND}
C {lab_pin.sym} -176.25 -340 2 0 {name=l25 sig_type=std_logic lab=SUB}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_var_hvt.sym} -375 -450 0 0 {name=C1 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_var_hvt.sym} -375 -345 0 0 {name=C2 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/nfet_01v8.sym} 1110 -660 0 0 {name=M25
L=0.15
W=0.42
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {lab_pin.sym} 1130 -596.25 0 0 {name=l26 sig_type=std_logic lab=GND}
C {lab_pin.sym} 1183.75 -660 2 0 {name=l27 sig_type=std_logic lab=SUB}
C {lab_pin.sym} 782.5 -875 1 0 {name=l28 sig_type=std_logic lab=vpwr}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 761.25 -811.25 0 0 {name=M26
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 891.25 -791.25 0 0 {name=M27
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 1111.25 -811.25 0 0 {name=M28
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 861.25 -671.25 0 0 {name=M29
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 971.25 -661.25 0 0 {name=M30
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_var_lvt.sym} 720 -630 0 0 {name=C3 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/nfet_01v8.sym} 1700 -640 0 0 {name=M31
L=0.15
W=0.42
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {lab_pin.sym} 1720 -576.25 0 0 {name=l29 sig_type=std_logic lab=GND}
C {lab_pin.sym} 1773.75 -640 2 0 {name=l30 sig_type=std_logic lab=SUB}
C {lab_pin.sym} 1372.5 -855 1 0 {name=l31 sig_type=std_logic lab=vpwr}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 1351.25 -791.25 0 0 {name=M32
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 1481.25 -771.25 0 0 {name=M33
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 1701.25 -791.25 0 0 {name=M34
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 1451.25 -651.25 0 0 {name=M35
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 1561.25 -641.25 0 0 {name=M36
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_var_lvt.sym} 1310 -610 0 0 {name=C4 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_var_lvt.sym} 620 -700 0 0 {name=C5 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_var_lvt.sym} 1270 -720 0 0 {name=C6 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/nfet_01v8.sym} 2410 -650 0 0 {name=M37
L=0.15
W=0.42
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {lab_pin.sym} 2430 -586.25 0 0 {name=l32 sig_type=std_logic lab=GND}
C {lab_pin.sym} 2483.75 -650 2 0 {name=l33 sig_type=std_logic lab=SUB}
C {lab_pin.sym} 2082.5 -865 1 0 {name=l34 sig_type=std_logic lab=vpwr}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 2061.25 -801.25 0 0 {name=M38
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 2191.25 -781.25 0 0 {name=M39
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 2411.25 -801.25 0 0 {name=M40
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 2161.25 -661.25 0 0 {name=M41
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 2271.25 -651.25 0 0 {name=M42
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_var_lvt.sym} 2020 -620 0 0 {name=C7 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/nfet_01v8.sym} 3000 -630 0 0 {name=M43
L=0.15
W=0.42
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {lab_pin.sym} 3020 -566.25 0 0 {name=l35 sig_type=std_logic lab=GND}
C {lab_pin.sym} 3073.75 -630 2 0 {name=l36 sig_type=std_logic lab=SUB}
C {lab_pin.sym} 2672.5 -845 1 0 {name=l37 sig_type=std_logic lab=vpwr}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 2651.25 -781.25 0 0 {name=M44
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 2781.25 -761.25 0 0 {name=M45
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 3001.25 -781.25 0 0 {name=M46
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 2751.25 -641.25 0 0 {name=M47
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 2861.25 -631.25 0 0 {name=M48
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_var_lvt.sym} 2610 -600 0 0 {name=C8 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_var_lvt.sym} 1920 -690 0 0 {name=C9 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_var_lvt.sym} 2570 -710 0 0 {name=C10 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/nfet_01v8.sym} 1090 -170 0 0 {name=M49
L=0.15
W=0.42
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {lab_pin.sym} 1110 -106.25 0 0 {name=l38 sig_type=std_logic lab=GND}
C {lab_pin.sym} 1163.75 -170 2 0 {name=l39 sig_type=std_logic lab=SUB}
C {lab_pin.sym} 762.5 -385 1 0 {name=l40 sig_type=std_logic lab=vpwr}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 741.25 -321.25 0 0 {name=M50
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 871.25 -301.25 0 0 {name=M51
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 1091.25 -321.25 0 0 {name=M52
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 841.25 -181.25 0 0 {name=M53
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 951.25 -171.25 0 0 {name=M54
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_var_lvt.sym} 700 -140 0 0 {name=C11 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/nfet_01v8.sym} 1680 -150 0 0 {name=M55
L=0.15
W=0.42
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {lab_pin.sym} 1700 -86.25 0 0 {name=l41 sig_type=std_logic lab=GND}
C {lab_pin.sym} 1753.75 -150 2 0 {name=l42 sig_type=std_logic lab=SUB}
C {lab_pin.sym} 1352.5 -365 1 0 {name=l43 sig_type=std_logic lab=vpwr}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 1331.25 -301.25 0 0 {name=M56
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 1461.25 -281.25 0 0 {name=M57
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 1681.25 -301.25 0 0 {name=M58
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 1431.25 -161.25 0 0 {name=M59
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 1541.25 -151.25 0 0 {name=M60
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_var_lvt.sym} 1290 -120 0 0 {name=C12 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_var_lvt.sym} 600 -210 0 0 {name=C13 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_var_lvt.sym} 1250 -230 0 0 {name=C14 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/nfet_01v8.sym} 2390 -160 0 0 {name=M61
L=0.15
W=0.42
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {lab_pin.sym} 2410 -96.25 0 0 {name=l44 sig_type=std_logic lab=GND}
C {lab_pin.sym} 2463.75 -160 2 0 {name=l45 sig_type=std_logic lab=SUB}
C {lab_pin.sym} 2062.5 -375 1 0 {name=l46 sig_type=std_logic lab=vpwr}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 2041.25 -311.25 0 0 {name=M62
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 2171.25 -291.25 0 0 {name=M63
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 2391.25 -311.25 0 0 {name=M64
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 2141.25 -171.25 0 0 {name=M65
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 2251.25 -161.25 0 0 {name=M66
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_var_lvt.sym} 2000 -130 0 0 {name=C15 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/nfet_01v8.sym} 2980 -140 0 0 {name=M67
L=0.15
W=0.42
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {lab_pin.sym} 3000 -76.25 0 0 {name=l47 sig_type=std_logic lab=GND}
C {lab_pin.sym} 3053.75 -140 2 0 {name=l48 sig_type=std_logic lab=SUB}
C {lab_pin.sym} 2652.5 -355 1 0 {name=l49 sig_type=std_logic lab=vpwr}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 2631.25 -291.25 0 0 {name=M68
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 2761.25 -271.25 0 0 {name=M69
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 2981.25 -291.25 0 0 {name=M70
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 2731.25 -151.25 0 0 {name=M71
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 2841.25 -141.25 0 0 {name=M72
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_var_lvt.sym} 2590 -110 0 0 {name=C16 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_var_lvt.sym} 1900 -200 0 0 {name=C17 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_var_lvt.sym} 2550 -220 0 0 {name=C18 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/nfet_01v8.sym} 1070 320 0 0 {name=M73
L=0.15
W=0.42
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {lab_pin.sym} 1090 383.75 0 0 {name=l50 sig_type=std_logic lab=GND}
C {lab_pin.sym} 1143.75 320 2 0 {name=l51 sig_type=std_logic lab=SUB}
C {lab_pin.sym} 742.5 105 1 0 {name=l52 sig_type=std_logic lab=vpwr}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 721.25 168.75 0 0 {name=M74
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 851.25 188.75 0 0 {name=M75
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 1071.25 168.75 0 0 {name=M76
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 821.25 308.75 0 0 {name=M77
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 931.25 318.75 0 0 {name=M78
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_var_lvt.sym} 680 350 0 0 {name=C19 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/nfet_01v8.sym} 1660 340 0 0 {name=M79
L=0.15
W=0.42
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {lab_pin.sym} 1680 403.75 0 0 {name=l53 sig_type=std_logic lab=GND}
C {lab_pin.sym} 1733.75 340 2 0 {name=l54 sig_type=std_logic lab=SUB}
C {lab_pin.sym} 1332.5 125 1 0 {name=l55 sig_type=std_logic lab=vpwr}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 1311.25 188.75 0 0 {name=M80
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 1441.25 208.75 0 0 {name=M81
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 1661.25 188.75 0 0 {name=M82
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 1411.25 328.75 0 0 {name=M83
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 1521.25 338.75 0 0 {name=M84
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_var_lvt.sym} 1270 370 0 0 {name=C20 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_var_lvt.sym} 580 280 0 0 {name=C21 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_var_lvt.sym} 1230 260 0 0 {name=C22 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/nfet_01v8.sym} 2370 330 0 0 {name=M85
L=0.15
W=0.42
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {lab_pin.sym} 2390 393.75 0 0 {name=l56 sig_type=std_logic lab=GND}
C {lab_pin.sym} 2443.75 330 2 0 {name=l57 sig_type=std_logic lab=SUB}
C {lab_pin.sym} 2042.5 115 1 0 {name=l58 sig_type=std_logic lab=vpwr}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 2021.25 178.75 0 0 {name=M86
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 2151.25 198.75 0 0 {name=M87
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 2371.25 178.75 0 0 {name=M88
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 2121.25 318.75 0 0 {name=M89
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 2231.25 328.75 0 0 {name=M90
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_var_lvt.sym} 1980 360 0 0 {name=C23 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/nfet_01v8.sym} 2960 350 0 0 {name=M91
L=0.15
W=0.42
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {lab_pin.sym} 2980 413.75 0 0 {name=l59 sig_type=std_logic lab=GND}
C {lab_pin.sym} 3033.75 350 2 0 {name=l60 sig_type=std_logic lab=SUB}
C {lab_pin.sym} 2632.5 135 1 0 {name=l61 sig_type=std_logic lab=vpwr}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 2611.25 198.75 0 0 {name=M92
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 2741.25 218.75 0 0 {name=M93
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 2961.25 198.75 0 0 {name=M94
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 2711.25 338.75 0 0 {name=M95
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sources/xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 2821.25 348.75 0 0 {name=M96
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_var_lvt.sym} 2570 380 0 0 {name=C24 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_var_lvt.sym} 1880 290 0 0 {name=C25 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_var_lvt.sym} 2530 270 0 0 {name=C26 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
